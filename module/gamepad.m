# gamepad.m
#
# Written in 2020 by David Boddie <david@boddie.org.uk>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

# Reads input from a gamepad/controller that can be accessed from outside the
# USB driver framework.

Gamepad: module
{
    PATH: con "/dis/lib/gamepad.dis";

    # Class (human interface, 3), Subclass (none, 0), protocol (none, 0)
    GamepadCSP: con int 16r000003;
    usbbase: con "/dev/usb";

    debug: int;

    GamePad: adt {
        dev: ref Usb->Dev;

        left, right, up, down: int;
        A, B, X, Y: int;
        leftsh, rightsh, start, select: int;

        open: fn(dev: ref Usb->Dev): ref GamePad;
        read: fn(g: self ref GamePad): int;
    };

    open: fn(file_name: string): ref GamePad;
};
