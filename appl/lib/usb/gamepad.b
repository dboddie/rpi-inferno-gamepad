# gamepad.b: USB HID gamepad/controller support for subclass=0, protocol=0 devices.
#
# Written in 2020 by David Boddie <david@boddie.org.uk>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

implement UsbDriver;

include "sys.m";
	sys: Sys;
	fprint, fildes: import sys;
include "string.m";
	str: String;
include "usb.m";
	usb: Usb;
	Dev, Ep: import usb;
        Ein, Eintr: import usb;

# Convenient variable for use with fprint
stderr: ref sys->FD;

# Class (human interface, 3), Subclass (none, 0), protocol (none, 0)
GamepadCSP      :con int 16r000003;

init(u: Usb, d: ref Dev): int
{
    # Record the Usb module for global use in the driver.
    usb = u;

    sys = load Sys Sys->PATH;
    str = load String String->PATH;

    # Obtain the USB device description.
    ud := d.usb;

#    stderr = fildes(2);
#    fprint(stderr, "Vendor: %x\nProduct: %x\n", ud.vid, ud.did);
#    fprint(stderr, "Vendor: %s\nProduct: %s\nSerial: %s\n", ud.vendor, ud.product, ud.serial);
#    fprint(stderr, "Class: %d\nSubclass: %d\nProtocol: %d\n", ud.class, ud.subclass, ud.proto);

    # There should be a configuration containing an interface with a human
    # interface class, no subclass and no protocol. The USB implementation
    # should have already enumerated all the endpoints.

    # Start looking for an endpoint.
    ep: ref Ep;
    in_ep: ref Ep;

    for (i := 0; i < len ud.ep; i++) {

        # Skip null endpoints.
        if ((ep = ud.ep[i]) == nil)
            continue;

        # Find the endpoint that sends incoming data which has the appropriate
        # class, subclass and interface protocol.
        if (ep.typ == Eintr && ep.dir == Ein && ep.iface.csp == GamepadCSP)
        {
            in_ep = ep;
#            fprint(stderr, "In: %x iface=%d\n", in_ep.addr, in_ep.iface.id);

            # Close the control and data files and let a separate tool reopen them.
            d.dfd = nil;
            d.cfd = nil;
            break;
        }
    }
    return 0;
}
shutdown()
{
}
