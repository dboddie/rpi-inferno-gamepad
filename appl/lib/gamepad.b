# gamepad.b
#
# Written in 2020 by David Boddie <david@boddie.org.uk>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

# Reads input from a gamepad/controller that can be accessed from outside the
# USB driver framework.

implement Gamepad;

include "sys.m";
    sys: Sys;

include "usb.m";
    usb: Usb;
    Dev, Ep, Usbdev: import usb;
    Ein, Eintr: import usb;
    Rh2d, Rd2h, Rclass, Rdev, Riface, Rsetconf, Rsetiface, Rstd: import usb;

include "readdir.m";
    readdir: Readdir;

include "gamepad.m";

# Convenient variable for use with fprint
stderr: ref sys->FD;

# Define an ADT to hold working information about the device.
Info: adt {
    # Control device file
    dev: ref Dev;
    # In and out endpoints
    in_ep: ref Ep;
    # Device files for in and out endpoints
    in_dev: ref Dev;
};

GamePad.open(dev: ref Dev): ref GamePad
{
    return ref GamePad(dev, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

GamePad.read(g: self ref GamePad): int
{
    maxpkt := g.dev.maxpkt;
    b := array[maxpkt] of byte;

    values := array[8] of int;
    offset := 0;

    while (offset < 8)
    {
        c := sys->read(g.dev.dfd, b, maxpkt - offset);
        if (c < 0) return 0;

        for (j := 0; j < c; j++) {
            values[offset] = int b[j];
            offset++;
        }
    }

    case values[0] {
    0 => g.left = 1; g.right = 0;
    255 => g.right = 1; g.left = 0;
    * => g.left = g.right = 0;
    }
    case values[1] {
    0 => g.up = 1; g.down = 0;
    255 => g.up = 0; g.down = 1;
    * => g.up = g.down = 0;
    }

    g.X = (values[5] & 16r10) >> 4;
    g.A = (values[5] & 16r20) >> 5;
    g.B = (values[5] & 16r40) >> 6;
    g.Y = (values[5] & 16r80) >> 7;

    g.leftsh = (values[6] & 16r01);
    g.rightsh = (values[6] & 16r02) >> 1;
    g.select = (values[6] & 16r10) >> 4;
    g.start = (values[6] & 16r20) >> 5;

    return 1;
}

# Get the USB device and return it, or nil if no suitable device could be found.

get_device(fname: string): ref Dev
{
    dev := usb->opendev(fname);
    if (dev == nil) {
        if (debug)
            sys->fprint(stderr, "gamepad: Failed to open %s\n", fname);
        return nil;
    }

    devdata := usb->opendevdata(dev, sys->ORDWR);
    if (devdata == nil) {
        if (debug)
            sys->fprint(stderr, "gamepad: Failed to open %s/data\n", fname);
        return nil;
    }

    if (usb->configdev(dev) < 0) {
        if (debug)
            sys->fprint(stderr, "gamepad: Failed to configure device %x\n", dev);
        return nil;
    }

    return dev;
}

# Find the endpoints for the specific type of device we support and return them
# in an Info ADT.

find_endpoints(dev: ref Dev): ref Info
{
    # There should be a configuration containing an interface with a human
    # interface device class and no subclass. The USB implementation should
    # have already enumerated all the endpoints.
    usbdev := dev.usb;

    # Start looking for an endpoint.
    ep: ref Ep;

    for (i := 0; i < len usbdev.ep; i++) {

        # Skip null endpoints.
        if ((ep = usbdev.ep[i]) == nil)
            continue;

        # Find the endpoint that sends incoming data which has the appropriate
        # class, subclass and interface protocol.
        if (ep.typ == Eintr && ep.dir == Ein && ep.iface.csp == GamepadCSP) {
            new_info := ref Info;
            new_info.in_ep = ep;
            new_info.dev = dev;
            return new_info;
        }
    }
    return nil;
}

# Find a gamepad device.

find_device(dirname: string): (ref Dev, ref Info)
{
    (entries, number) := readdir->init(dirname, readdir->NAME);
    if (number == -1)
        return (nil, nil);

    for (i := 0; i < number; i++)
    {
        if (entries[i].qid.qtype != Sys->QTDIR)
            continue;

        dev := get_device(dirname + "/" + entries[i].name);
        if (dev == nil)
            continue;

        inf := find_endpoints(dev);
        if (inf != nil)
            return (dev, inf);
    }

    return (nil, nil);
}

# Set (select) the first configuration provided by the device.
setfirstconfig(dev: ref Dev): int
{
    # Host to device, standard, device request
    # request=set configuration, value=1, index=0, no data
    nr := usb->usbcmd(dev, Rh2d | Rstd | Rdev, Rsetconf, 1, 0, nil, 0);
    if (nr < 0) return -1;

    # Device to host, standard, interface request
    # request=get descriptor, 
    #nr = usb->usbcmd(dev, Rd2h | Rstd | Riface, Rgetdesc, Dreport<<8, id, desc, descsz);
    #sys->fprint(sys->fildes(2), "get desc: %d\n", nr);
    #if (nr <= 0) return -1;

    #sys->fprint(sys->fildes(2), dev.

    return nr;
}


# The open function finds a device and its endpoints and returns an object to
# handle interaction with the device.
open(file_name: string): ref GamePad
{
    sys = load Sys Sys->PATH;
    usb = load Usb Usb->PATH;
    readdir = load Readdir Readdir->PATH;
    stderr = sys->fildes(2);
    usb->init();

    dev: ref Dev;
    info: ref Info;

    if (file_name != nil)
    {
        dev = get_device(file_name);
        if (dev == nil) {
            sys->fprint(stderr, "gamepad: No Usbdev for %s\n", file_name);
            return nil;
        }

        # Define the global info object representing the device.
        info = find_endpoints(dev);
        if (info == nil) {
            sys->fprint(stderr, "gamepad: Not a gamepad: %s\n", file_name);
            return nil;
        }
    }
    else
    {
        (dev, info) = find_device(usbbase);
        if (dev == nil || info == nil) {
            sys->fprint(stderr, "Failed to find a gamepad.\n");
            return nil;
        }
    }

    setfirstconfig(dev);

    # Create device files for endpoints by using the setup endpoint for the
    # device to specify the interrupt in endpoint.
    info.in_dev = usb->openep(dev, info.in_ep.id);
    if (info.in_dev == nil) {
        sys->fprint(stderr, "gamepad: Failed to open endpoint for reading: %d\n", info.in_ep.id);
        return nil;
    }

    usb->opendevdata(info.in_dev, sys->OREAD);
    if (info.in_dev.dfd == nil) {
        sys->fprint(stderr, "gamepad: Failed to open device file for reading\n");
        return nil;
    }

    if (info.in_dev.maxpkt > 8) {
        sys->fprint(stderr, "gamepad: Maximum packet size too large: %d\n", info.in_dev.maxpkt);
        return nil;
    }

    return GamePad.open(info.in_dev);
}
