# gamepad.b
#
# Written in 2020 by David Boddie <david@boddie.org.uk>
#
# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.
#
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

# Reads input from a gamepad/controller that can be accessed from outside the
# USB driver framework.

implement GamepadTest;

include "sys.m";
sys: Sys;
include "draw.m";
include "usb.m";
include "gamepad.m";

GamepadTest: module
{
    init: fn(ctxt: ref Draw->Context, args: list of string);
};

init(nil: ref Draw->Context, nil: list of string)
{
    sys = load Sys Sys->PATH;
    gpad := load Gamepad Gamepad->PATH;
    GamePad: import gpad;

    gamepad := gpad->open(nil);
    if (gamepad == nil) {
        sys->fprint(sys->fildes(2), "Cannot open gamepad.\n");
        exit;
    }

    c := 0;

    while (c < 1000) {
        if (!gamepad.read()) break;

        if (gamepad.left) c += print("L ");
        if (gamepad.right) c += print("R ");
        if (gamepad.up) c += print("U ");
        if (gamepad.down) c += print("D ");

        if (gamepad.A) c += print("A ");
        if (gamepad.B) c += print("B ");
        if (gamepad.X) c += print("X ");
        if (gamepad.Y) c += print("Y ");

        if (gamepad.leftsh) c += print("LS ");
        if (gamepad.rightsh) c += print("RS ");
        if (gamepad.select) c += print("SL ");
        if (gamepad.start) c += print("ST ");
    }
}

print(text: string): int
{
    sys->print("%s", text);
    return 1;
}
