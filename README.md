# Gamepad support for Inferno

USB HID gamepad/controller support for subclass=0, protocol=0 devices when
used with the Raspberry Pi port of Inferno.

Please read `lib/usbdb.new` and copy the appropriate lines into your system's own
`/lib/usbdb` file.

## Notes on the implementation

The USB subsystem uses the `/lib/usbdb` file to define where to look for
drivers when it sees new devices. In the gamepad repository, the fragment in
the `/lib/usbdb.new` file shows the definition for the gamepad.

The driver is located in `/dis/lib/usb/gamepad.dis` so the source file that
must be built is in `/appl/lib/usb/gamepad.b`. The driver implements
`UsbDriver` (see `/module/usb.m`) and provides `init` and `shutdown`
functions, though the `shutdown` function is empty.

All the gamepad driver does is look for the device and close its endpoints so
that another tool can work with them. I think, by default, the USB drivers
conventionally keep running as child processes of the USB daemon. I wanted
something that didn't run all the time.

As a result, I wrote a library in `/appl/lib/gamepad.b` to look for the
gamepad and access its endpoints. If they aren't closed in the driver then
they aren't accessible in a library unless you provide a mechanism to access
them.

The program in `/appl/cmd/usb/gamepad.b` uses the library to access the
gamepad once the driver has seen it. In theory, it should just work. A lot of
the code to talk to the device is in the library.

## License

    Written in 2020 by David Boddie <david@boddie.org.uk>

    To the extent possible under law, the author(s) have dedicated all copyright
    and related and neighboring rights to this software to the public domain
    worldwide. This software is distributed without any warranty.

    You should have received a copy of the CC0 Public Domain Dedication along with
    this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
